<!DOCTYPE html>
<html lang="en">

<head>
    <title>Nyalinin - Slicing Design to Website</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Nyalinin Media">
    <meta name="keywords" content="Nyalin, Nyalinin Media, Slicing Figma to Website">

    <meta property="og:title" content="nyalin.in" />
    <meta property="og:description" content="Nyalinin - Slicing Design to Website" />
    <meta property="og:url" content="https://www.nyalin.in/" />
    <meta property="og:image" content="./2022/assets/images/hg_lms.jpg" />

    <link rel="Shortcut icon" href="./2022/assets/images/favicon.ico">
    <!-- <link rel="stylesheet" href="assets/css/main.css?<?= time() ?>" /> -->
    <link rel="stylesheet" href="assets/css/splide.min.css?<?= time() ?>">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css?<?= time() ?>"> -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@3/dist/js/splide.min.js"></script>
    <!-- <script src="assets/js/bootstrap.bundle.min.js?<?= time() ?>"></script> -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- tailwind -->
    <link rel="stylesheet" href="src/tailwind.css?<?= time() ?>" />
    <script src="https://unpkg.com/flowbite@1.4.1/dist/flowbite.js"></script>
    <script src="../path/to/flowbite/dist/flowbite.min.js"></script>
    <!-- end tailwind -->

</head>

<body>

    <section class="splide" aria-label="Splide Basic HTML Example">
        <div class="splide__track">
            <ul class="splide__list">
                <li class="splide__slide">
                    <img src="assets/images/s1/bg-1.png" class="min-h-[100vh] object-cover">
                    <div
                        class='w-full max-w-5xl m-auto bg-slate-400 relative top-[-150px] text-white text-center'>
                        <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js
                            AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js
                            AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
                    </div>
                </li>

                <li class="splide__slide">
                    <img src="assets/images/s1/bg-1.png" class="min-h-[100vh] object-cover">
                    <div
                        class='bg-slate-400 absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] text-white text-center'>
                        AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js
                    </div>
                </li>

                <li class="splide__slide">
                    <img src="assets/images/s1/bg-1.png" class="min-h-[100vh] object-cover">
                    <div
                        class='bg-slate-400 absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] text-white text-center'>
                        AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js
                    </div>
                </li>
            </ul>
        </div>
    </section>

    <!-- 
    <div class='w-full font-Montserrat'>
        <div class='flex flex-nowrap overflow-auto h-10'>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
            <div>AAAAAAAAAhttps://unpkg.com/flowbite@1.4.1/dist/flowbite.js</div>
        </div>
    </div> -->

</body>

</html>

<script>
var splide = new Splide('.splide', {
    perPage: 1,
    rewind: true,
    autoplay: true,
    breakpoints: {
        991.98: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
        768: {
            perPage: 1,
            gap: "1rem",
            // destroy: true,
        },
    },
});

splide.mount();
</script>
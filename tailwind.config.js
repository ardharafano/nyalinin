/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{html,js}', './index.php', './node_modules/flowbite/**/*.js'],
    theme: {
        extend: {},
        fontFamily: {

            'Montserrat': ['Montserrat', 'sans-serif'],
        }
    },
    plugins: [
        require('flowbite/plugin')
    ]
}